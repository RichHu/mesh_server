Node.js 安裝與環境設定
* 使用Brew進行安裝
    - brew install node
* 安裝MQTT module
    - npm install mqtt //*** 本地安裝(ps. 請到有node_modules的目錄下eg. /meshExpressApp, meshDemo/)
    - npm install mqtt -g  //*** global 安裝
* mariadb(https://blog.gtwang.org/linux/mysql-create-database-add-user-table-tutorial/)
    - 參考網址二: https://www.jianshu.com/p/381888e3a67b
    - 安裝: brew install mariadb (10.4.6)
    - 啟動: brew services start mariadb
* 連接DB: 
    - mysql -uroot
    - ERROR 1698 (28000): Access denied for user 'root'@‘localhost’
    - sudo mysql_secure_installation //*** 記得設定要設定密碼, 幾乎都回答yes
    - mysql -u root -p //*** 即可連接DB
* 建立資料庫: eg. mysql> CREATE DATABASE `my_db`;
* 使用建立的資料庫 eg. mysql> use my_db;
* 針對node.js安裝mariasql: 
    - Install  mariasql(ps. 壓縮包已經ready)
    - npm install mariasql
* 連線遠端資料庫需特別設定開放權限, 否則會連線失敗.(https://www.ewdna.com/2011/09/mysqlmessage-from-server-host-xxx-is.html)
* 遠端資料庫權限開放範例如下：
    - 以root身份登入db. eg. shell> mysql -u root
    - 使用已建立的db.eg. mysql> use my_db;
    - 開放權限eg. mysql> GRANT ALL ON db_name.* TO my_user@‘192.168.%.%' identified by ‘my_password’;
    - 確認設定生效eg. mysql> FLUSH PRIVILEGES;