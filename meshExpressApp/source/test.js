console.debug("Hello world...")
//链接Mariasql
var Client = require('mariasql');

// //配置相关信息
var c = new Client({
  host: '127.0.0.1',
  //用户名
  user: 'my_user',
  //密码默认为空
  password: 'my_password',
  //使用哪个数据库
  db: 'my_db'
});

// c.connect();

c.query('SHOW DATABASES', function(err, rows) {
  if (err)
    throw err;
    console.log('---------查看所有的数据库------------');
    console.dir(rows);
});


//使用array的形式快于对象,效果一样
c.query('SHOW TABLES', null, { useArray: true }, function(err, rows) {
  if (err)
    throw err;
  console.log('--------查看所有的数据表格-------------');
  console.dir(rows);
});

//结合使用SQL语句
c.query('SELECT * FROM orders', function(err, rows) {
  if (err)
    throw err;
  console.log('--------查看orders的数据-------------');
  console.dir(rows);
});

//使用占位符
c.query('SELECT * FROM orders WHERE order_id = ? AND customer_id = ?',
        [ 5, 5 ],
        function(err, rows) {
  if (err)
    throw err;
  console.log('--------SELECT + WHERE-------------');   
  console.dir(rows);
});

//定义sql语句, 稍后使用,另外一种占位符
var prep = c.prepare('SELECT * FROM orders WHERE order_id = :orderid AND customer_id = :customerid');
c.query(prep({orderid: 5, customerid: 5}), function(err, rows){
    if (err) {
        throw err
    } else {
         console.log('--------SELECT + WHERE-------------');    
        console.dir(rows);
    }
})


//关闭数据库
c.end();
