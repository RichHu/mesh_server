var http = require("http");
var url = require("url");
// var exec = require("child_process").exec;

function start(route, handle) {
    function onRequest(request, response) {
        // var content = "empty";
        // exec("ls -lah", function (error, stdout, stderr) {
        //     response.writeHead(200, {"Content-Type": "text/plain"});
        //     response.write(stdout);
        //     response.end();
        //   });
        var pathname = url.parse(request.url).pathname;
        route(handle, pathname, response);
        // response.writeHead(200, {"Content-Type": "text/plain"});
        // response.write(content);
        // response.end();
        // execute(function(word){ console.log(word) }, "Hello");
      }
      http.createServer(onRequest).listen(8888);
      console.log("Server has started.");
  }
  
  exports.start = start;