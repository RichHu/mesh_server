// var express = require('express');
// var router = express.Router();

// /* GET home page. */
// router.get('../source/server.js', function(req, res, next) {
//   console.log("router.get...trigger")
//   res.render('index', { title: 'Express' });
// });
// module.exports = router;

var server = require("../source/server.js");
var router = require("../source/route.js");
var requestHandlers = require("../source/requestHandler.js");

var handle = {}
handle["/"] = requestHandlers.start;
handle["/start"] = requestHandlers.start;
handle["/upload"] = requestHandlers.upload;

server.start(router.route, handle);
