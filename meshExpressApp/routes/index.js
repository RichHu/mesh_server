var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('../source/server.js', function(req, res, next) {
  console.log("router.get...trigger")
  res.render('index', { title: 'Express' });
});
module.exports = router;