var express = require('express');
var app = express();
var exec = require("child_process").exec;
app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded({ extended: true }));

// var nodemailer = require('nodemailer');
// var smtpTransport = require('nodemailer-smtp-transport');

// var mailTransport = nodemailer.createTransport('SMTP', {
//     service: 'Gmail',
//     auth: {
//       // user: credentials.gmail.user,
//       // pass: credentials.gmail.password
//       user: 'reach.hu@gmail.com',
//       pass: 'reach8227122'
//     }
//   });

// module.exports = {
//     cookieSecret: 'your cookie secret goes here',
//     gmail: {
//       user: 'reach.hu@gmail.com',
//       pass: 'reach8227122'
//     }
//   };

// var bodyParser = require('body-parser');
// app.use(bodyParser.json({limit: '1mb'}));  //body-parser 解析json格式数据
// app.use(bodyParser.urlencoded({            //此项必须在 bodyParser.json 下面,为参数编码
//   extended: true
// }));

var server = app.listen(8081, function () {
 
    var host = server.address().address
    var port = server.address().port
   
    console.log("应用实例，访问地址为 http://%s:%s", host, port)
   
  })

//  主页输出 "Hello World"
app.get('/', function (req, res) {
   console.log("主页 GET 请求");
//    res.send('Hello GET');
   exec("ls -lah", function (error, stdout, stderr) {
    // res.send(200, {"Content-Type": "text/plain"});
    res.send(stdout);
    res.end();
  });
})
 
 
//  POST 请求
app.post('/', function (req, res) {
   console.log("主页 POST 请求");
   res.send('Hello POST');
})
 
//  /del_user 页面响应
app.get('/del_user', function (req, res) {
   console.log("/del_user 响应 DELETE 请求");
   res.send('删除页面');
})
 
//  /list_user 页面 GET 请求
app.get('/list_user', function (req, res) {
   console.log("/list_user GET 请求");
   res.send('用户列表页面');
})
 
// 对页面 abcd, abxcd, ab123cd, 等响应 GET 请求
app.get('/ab*cd', function(req, res) {   
   console.log("/ab*cd GET 请求");
   res.send('正则匹配');
})

//链接Mariasql
var Client = require('mariasql');

// //配置相关信息
var c = new Client({
  host: '127.0.0.1',
  //用户名
  user: 'my_user',
  //密码默认为空
  password: 'my_password',
  //使用哪个数据库
  db: 'my_db'
});

// c.connect();
//  /list_user 页面 GET 请求
app.post('/database', function (req, res) {
    console.log("/show_database post 请求");
    // self.handle_show_database(res)
    c.query('SHOW DATABASES', function(err, rows) {
        if (err)
          throw err;
        //   console.log('---------查看所有的数据库------------');
        //   console.dir(rows);
        //   res.send('---------查看所有的数据库------------');
          res.send(rows);
      });
 })

//  local function handle_show_database(){
//     c.query('SHOW DATABASES', function(err, rows) {
//         if (err)
//           throw err;
//           console.log('---------查看所有的数据库------------');
//         //   console.dir(rows);
//         //   res.send('---------查看所有的数据库------------');
//           res.send(rows);
//       });
//  }

app.post('/mail_new', function(req, res, next){
    var name = req.body.name
    var email = req.body.email
    console.log("/param post sendmail..."+ name + ':' + email);
    try{
        let mailer = require('nodemailer')
        let smtpTransport = require('nodemailer-smtp-transport')
    
        let options = {
          service: 'gmail',
          secure: true,
          auth: {
            user: 'reach.hu@gmail.com',
            pass: 'reach8227122'
          }
        }
    
        let transport = mailer.createTransport(smtpTransport(options))
    
        let mail = {
          from: name,//'from',
          to: email,//'reach.hu@gmail.com',
          subject: 'subject',
          html: 'I\'m CyberTan ios developer...' + name//'body'
        }
    
        transport.sendMail(mail, (error, response) => {
          transport.close()
          if (error) {
            // res.json(400, {error});
            res.status(400).json({error})
          } else {
            // res.json(200, {response: 'Mail sent.'});
            res.status(200).json({response: 'Mail sent.'})
          }
        })
      } catch(error) {
        // res.json(400, {error});
        res.status(400).json({error})
      }
       
})

app.post('/mail', function(req, res, next){
    let nodemailer = require('nodemailer')
    let smtpTransport = require('nodemailer-smtp-transport')
    var name = req.body.name
    var email = req.body.email
    console.log("/param post sendmail..."+ name + ':' + email);

    // var mailTransport = nodemailer.createTransport("smtps://reach.hu@gmail.com:"+encodeURIComponent('reach8227122') + "@smtp.gmail.com:465");
    
    let options = {
          service: 'gmail',
          secure: true,
          auth: {
            user: 'reach.hu@gmail.com',
            pass: 'reach8227122'
          }
        }
    
    let mailTransport = nodemailer.createTransport(smtpTransport(options))

    let mail = {
        from: name,//'from',
        to: email,//'reach.hu@gmail.com',
        subject: 'subject',
        html: 'I\'m CyberTan ios developer...' + name//'body'
      }
  
      mailTransport.sendMail(mail, (error, response) => {
        mailTransport.close()
        if (error) {
          // res.json(400, {error});
          res.status(400).json({error})
        } else {
          // res.json(200, {response: 'Mail sent.'});
          res.status(200).json({response: 'Mail sent.'})
        }
      })
})

app.post('/param', function (req, res, next) {
    console.log("/param post 请求");
    console.log('--------Test param-------------');
        // // var adminPass = req.body.data
        // var body = req.body
        // // var adminPass = "test"
        // console.log(body)
        // var str_res = "Your param password = " + body.data.adminPass
        // res.send(str_res);
        if (req.body.command) {
            //能正确解析 json 格式的post参数
            console.log('能 - 正确解析 json 格式的post参数');
            // res.send({"status": "success1", "command": req.body.command, "data": req.body.data});
            res.send({"status": "success1", "name": req.body.name, "email": req.body.email});
        } else {
            // 不能正确解析json 格式的post参数
            console.log('不能 - 正确解析json 格式的post参数');
            var body = '', jsonStr;
            req.on('data', function (chunk) {
                console.log("req.on(data)...triger"+chunk)
                body += chunk; //读取参数流转化为字符串
            });
            req.on('end', function () {
                //读取参数流结束后将转化的body字符串解析成 JSON 格式
                console.log("req.(end)...triger"+chunk)
                try {
                    jsonStr = JSON.parse(body);
                } catch (err) {
                    jsonStr = null;
                }
                jsonStr ? res.send({"status":"success2", "command": jsonStr.command, "data": jsonStr.data}) : res.send({"status":"error"});
            });
            res.end();
            // try {
            //     jsonStr = JSON.parse(body);
            // } catch (err) {
            //     jsonStr = null;
            // }
            // jsonStr ? res.send({"status":"success2", "command": jsonStr.command, "data": jsonStr.data}) : res.send({"status":"error"});
        }
 })

 app.post('/tables', function (req, res) {
    console.log("/show_tables post 请求");
    c.query('SHOW TABLES', null, { useArray: true }, function(err, rows) {
        if (err)
          throw err;
        console.log('--------查看所有的数据表格-------------');
        res.send(rows);
      });
 })

 app.post('/orders', function (req, res) {
    console.log("/orders_all POST 请求");
    c.query('SELECT * FROM orders', function(err, rows) {
        if (err)
          throw err;
        console.log('--------查看orders的数据-------------');
        res.send(rows);
      });
 })

 var mqtt = require('mqtt')
var fs = require('fs');
var opt = {
    port:8883,
    clientId:'mqttjs_' + Math.random().toString(16).substr(2, 8),
    username: 'CMCC_MW1M',
    password: '123654790',
    clean:true,
    rejectUnauthorized: false
    // ca: fs.readFileSync('/Users/ilovecybertan/Projects/web/server/meshDemo/ca_certificates/ca.crt')
}

var client = mqtt.connect('mqtts://192.168.88.100', opt);
client.on('connect', function(){
    console.log('已連接至MQTT server');
    client.subscribe('test/yard');
    client.subscribe('$willMsg/#');
    client.subscribe('$devMsg/#');
    client.subscribe('devMsg');
});

client.on('message', function(topic, msg){
    console.log('收到'+topic+'訊息'+msg)
})

const tls = require('tls');
