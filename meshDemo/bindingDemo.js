var express = require('express');
var app = express();
var exec = require("child_process").exec;
app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded({ extended: true }));
// var http_host = '192.168.1.82';
var http_port = 8081;
var mqtt_protocol = 'mqtts'; //ps. use mqtts:// for security conn, mqtt:// is for non-secure conn.
var mqtt_host = '192.168.1.51'; 
var mqtt_port = 8883;
var mqtt_account = 'cbt_user';
var mqtt_password = '12345678';
var db_host = '192.168.1.51';
var db_name = 'my_db';
var db_account = 'my_user';
var db_password = 'my_password';
var mail_account = 'cybertan.mqtt@gmail.com';
var mail_password = 'my_cybertan_mqtt';

var mqtt = require('mqtt')
var fs = require('fs');
var opt = {
    port:mqtt_port,
    clientId:'mqttjs_' + Math.random().toString(16).substr(2, 8),
    username:'admin',//mqtt_account,
    password:'cybertan',//mqtt_password,
    clean:true,
    rejectUnauthorized: false,
    // ca: fs.readFileSync('/Users/ilovecybertan/Projects/web/server/meshDemo/ca_certificates/ca.crt')
    ca: fs.readFileSync('./ca_certificates/ca.crt'),
    will:{topic:'$willMsg/httpServer', payload:'http is disconnected!', qos:1}
}
var mqtt_url = mqtt_protocol + '://' + mqtt_host;
console.log('mqtt_url = ' + mqtt_url);
var client = mqtt.connect(mqtt_url, opt);
client.on('connect', function(){
    console.log('已連接至MQTT server');
    // client.subscribe('test/yard');
    client.subscribe('willMsg/#');
    // client.subscribe('$devMsg/#');
    // client.subscribe('devMsg');
    client.subscribe('test/yard');
    // client.subscribe('$willMsg/+');
    client.subscribe('devMsg/#');
});

client.on('message', function(topic, msg){
    console.log('收到'+topic+'訊息'+msg)
})

var server = app.listen(http_port, function () {
 
    var host = server.address().address
    var port = server.address().port
   
    console.log("应用实例，访问地址为 http://%s:%s", host, port)
  })

//  主页输出 "Hello World"
app.get('/', function (req, res) {
   console.log("主页 GET 请求");
//    res.send('Hello GET');
   exec("ls -lah", function (error, stdout, stderr) {
    // res.send(200, {"Content-Type": "text/plain"});
    res.send(stdout);
    res.end();
  });
})
 
 
//  POST 请求
app.post('/', function (req, res) {
   console.log("主页 POST 请求");
   res.send('Hello POST');
})
 
//  /del_user 页面响应
app.get('/del_user', function (req, res) {
   console.log("/del_user 响应 DELETE 请求");
   res.send('删除页面');
})
 
//  /list_user 页面 GET 请求
app.get('/list_user', function (req, res) {
   console.log("/list_user GET 请求");
   res.send('用户列表页面');
})
 
// 对页面 abcd, abxcd, ab123cd, 等响应 GET 请求
app.get('/ab*cd', function(req, res) {   
   console.log("/ab*cd GET 请求");
   res.send('正则匹配');
})

//链接Mariasql
var Client = require('mariasql');

// //配置相关信息
var c = new Client({
  host: db_host,
  //用户名
  user: db_account,
  //密码默认为空
  password: db_password,
  //使用哪个数据库
  db: db_name
});

// c.connect();
//  /list_user 页面 GET 请求
app.post('/database', function (req, res) {
    console.log("/show_database post 请求");
    // self.handle_show_database(res)
    c.query('SHOW DATABASES', function(err, rows) {
        if (err)
          throw err;
        //   console.log('---------查看所有的数据库------------');
        //   console.dir(rows);
        //   res.send('---------查看所有的数据库------------');
          res.send(rows);
      });
 })

//  local function handle_show_database(){
//     c.query('SHOW DATABASES', function(err, rows) {
//         if (err)
//           throw err;
//           console.log('---------查看所有的数据库------------');
//         //   console.dir(rows);
//         //   res.send('---------查看所有的数据库------------');
//           res.send(rows);
//       });
//  }

app.post('/mail_new', function(req, res, next){
    var name = req.body.name
    var email = req.body.email
    console.log("/param post sendmail..."+ name + ':' + email);
    try{
        let mailer = require('nodemailer')
        let smtpTransport = require('nodemailer-smtp-transport')
    
        let options = {
          service: 'gmail',
          secure: true,
          auth: {
            user: mail_account,
            pass: mail_password
          }
        }
    
        let transport = mailer.createTransport(smtpTransport(options))
    
        let mail = {
          from: name,//'from',
          to: email,//'reach.hu@gmail.com',
          subject: 'subject',
          html: 'I\'m CyberTan ios developer...' + name//'body'
        }
    
        transport.sendMail(mail, (error, response) => {
          transport.close()
          if (error) {
            // res.json(400, {error});
            res.status(400).json({error})
          } else {
            // res.json(200, {response: 'Mail sent.'});
            res.status(200).json({response: 'Mail sent.'})
          }
        })
      } catch(error) {
        // res.json(400, {error});
        res.status(400).json({error})
      }
       
})

app.post('/mail', function(req, res, next){
    let nodemailer = require('nodemailer')
    let smtpTransport = require('nodemailer-smtp-transport')
    var name = req.body.name
    var email = req.body.email
    console.log("/param post sendmail..."+ name + ':' + email);

    // var mailTransport = nodemailer.createTransport("smtps://reach.hu@gmail.com:"+encodeURIComponent('reach8227122') + "@smtp.gmail.com:465");
    
    let options = {
          service: 'gmail',
          secure: true,
          auth: {
            user: mail_account,
            pass: mail_password
          }
        }
    
    let mailTransport = nodemailer.createTransport(smtpTransport(options))

    let mail = {
        from: name,//'from',
        to: email,//'reach.hu@gmail.com',
        subject: 'subject',
        html: 'I\'m CyberTan ios developer...' + name//'body'
      }
  
      mailTransport.sendMail(mail, (error, response) => {
        mailTransport.close()
        if (error) {
          // res.json(400, {error});
          res.status(400).json({error})
        } else {
          // res.json(200, {response: 'Mail sent.'});
          res.status(200).json({response: 'Mail sent.'})
        }
      })
})

app.post('/param', function (req, res, next) {
    console.log("/param post 请求");
    console.log('--------Test param-------------');
        // // var adminPass = req.body.data
        // var body = req.body
        // // var adminPass = "test"
        // console.log(body)
        // var str_res = "Your param password = " + body.data.adminPass
        // res.send(str_res);
        if (req.body.command) {
            //能正确解析 json 格式的post参数
            console.log('能 - 正确解析 json 格式的post参数');
            // res.send({"status": "success1", "command": req.body.command, "data": req.body.data});
            res.send({"status": "success1", "name": req.body.name, "email": req.body.email});
        } else {
            // 不能正确解析json 格式的post参数
            console.log('不能 - 正确解析json 格式的post参数');
            var body = '', jsonStr;
            req.on('data', function (chunk) {
                console.log("req.on(data)...triger"+chunk)
                body += chunk; //读取参数流转化为字符串
            });
            req.on('end', function () {
                //读取参数流结束后将转化的body字符串解析成 JSON 格式
                console.log("req.(end)...triger"+chunk)
                try {
                    jsonStr = JSON.parse(body);
                } catch (err) {
                    jsonStr = null;
                }
                jsonStr ? res.send({"status":"success", "command": jsonStr.command, "data": jsonStr.data}) : res.send({"status":"error"});
            });
            res.end();
            // try {
            //     jsonStr = JSON.parse(body);
            // } catch (err) {
            //     jsonStr = null;
            // }
            // jsonStr ? res.send({"status":"success", "command": jsonStr.command, "data": jsonStr.data}) : res.send({"status":"error"});
        }
 })

 app.post('/tables', function (req, res) {
    console.log("/show_tables post 请求");
    c.query('SHOW TABLES', null, { useArray: true }, function(err, rows) {
        if (err)
          throw err;
        console.log('--------查看所有的数据表格-------------');
        res.send(rows);
      });
 })

 app.post('/orders', function (req, res) {
    console.log("/orders_all POST 请求");
    c.query('SELECT * FROM orders', function(err, rows) {
        if (err)
          throw err;
        console.log('--------查看orders的数据-------------');
        res.send(rows);
      });
 })

// const tls = require('tls');

/**
 * Binding(Register)
 */
app.post('/cytertan_demo/register', function(req, res, next){

  if (req.body.command) {
    //能正确解析 json 格式的post参数
    var name = req.body.data.name;
    var email = req.body.data.email;
    var password = req.body.data.password;
    var deviceId = req.body.data.deviceId;
    var status = 0
    var verifyCode = Math.random().toString(10).substring(2, 5) + Math.random().toString(10).substring(2, 5);
    var sessionId = "NA"
    console.log("verifyCode = "+verifyCode)
    console.log('req.body.command('+req.body.command+'):'+name+','+email+','+password+','+deviceId+',');
    c.query('SELECT * FROM test_accounts where email = ?',[email], function(err, rows) {
      if (err){
        console.log('throw err!');
        res.send({"status": "fail"});
        res.end();
        throw err;
      }
      console.log('Check row...');
      let numRows = rows.length;
      if (numRows == 0){
        console.log('rows = '+numRows);
        c.query("INSERT INTO test_accounts (name, email, password, status, deviceId, verifyCode, sessionId) VALUES (?, ?, ?, ?, ?, ?, ?)",[name, email, password, status, deviceId, verifyCode, sessionId], function(err, rows) {
          if (err){
            console.log('throw err!%s', rows);
            res.send({"status": "fail"});
            res.end();
            throw err;
          }
          message = "The CyberTan verified code - "+verifyCode
          sendMail(email, message, res)
        });
      }else{
        console.log('email already exist');
        res.send({"status": "fail", 
                  "desc":"email already exist"});
        res.end();
      }
    });
    
  } else {
    // 不能正确解析json 格式的post参数
    console.log('不能 - 正确解析json 格式的post参数');
    var body = '', jsonStr;
    req.on('data', function (chunk) {
        console.log("req.on(data)...triger"+chunk)
        body += chunk; //读取参数流转化为字符串
    });
    req.on('end', function () {
        //读取参数流结束后将转化的body字符串解析成 JSON 格式
        console.log("req.(end)...triger"+chunk)
        try {
            jsonStr = JSON.parse(body);
        } catch (err) {
            jsonStr = null;
        }
        jsonStr ? res.send({"status":"success", "command": jsonStr.command, "data": jsonStr.data}) : res.send({"status":"error"});
    });
    res.end();
  }
})

/**
 * Get random char string.
 */
function makeid(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

/**
 * Login
 */
app.post('/cytertan_demo/login', function(req, res, next){

  if (req.body.command) {
    //能正确解析 json 格式的post参数
    // var name = req.body.data.name;
    var email = req.body.data.email;
    var password = req.body.data.password;
    // var deviceId = req.body.data.deviceId;
    var status = 0
    // console.log('req.body.command('+req.body.command+'):'+name+','+email+','+password+','+deviceId+',');
    console.log('req.body.command('+req.body.command+'):'+email+','+password+',');
    c.query('SELECT * FROM test_accounts where email = ?',[email], function(err, rows) {
      if (err){
        console.log('throw err!');
        res.send({"status": "fail",
                  "desc":"Not register!"});
        res.end();
        // throw err;
        return;
      }
      console.log('Check row...');
      let numRows = rows.length;
      if (numRows != 0){
        console.log('rows = %s',rows);
        if(rows[0].status == 0){
          console.log('account is not verified.');
          res.send({"status": "fail", 
                  "desc":"account is not verified."});
          res.end();
        }else if(rows[0].status == -1){
          console.log('account is deleted.');
          res.send({"status": "fail", 
                  "desc":"account is deleted."});
          res.end();
        }else if(rows[0].status == 1 || rows[0].status == 2){
          console.log('Account status check ok');
          if (rows[0].password == password){
            var sessionId = makeid(16);
            console.log('Password check ok session is '+sessionId);
            c.query("UPDATE test_accounts SET status = 2, sessionId = ? WHERE id = ?",[sessionId, rows[0].id], function(err, rows) {
            // c.query("UPDATE test_accounts SET sessionId = 1234 WHERE id = ?", [rows[0].id], function(err, rows) {
              if (err){
                console.log('throw err!');
                res.send({
                  "status": "fail",
                  "desc":"SQL: update status fail"});
                res.end();
                throw err;
              }
              console.log('login ok.');
              res.send({"status": "success", 
                    "sessionId":sessionId});
              res.end();
            });
            // res.send({"status": "success", 
            //       "session":"listene device ok.",
            //     });
            // res.end();
          }
        }else{
          console.log('account status is invalid.');
          res.send({"status": "fail", 
                  "desc":"account status is invalid."});
          res.end();
        }
      }else{
        console.log('account is not exist');
        res.send({"status": "fail", 
                  "desc":"account is not exist."});
        res.end();
      }
    });
    
  } else {
    // 不能正确解析json 格式的post参数
    console.log('不能 - 正确解析json 格式的post参数');
    var body = '', jsonStr;
    req.on('data', function (chunk) {
        console.log("req.on(data)...triger"+chunk)
        body += chunk; //读取参数流转化为字符串
    });
    req.on('end', function () {
        //读取参数流结束后将转化的body字符串解析成 JSON 格式
        console.log("req.(end)...triger"+chunk)
        try {
            jsonStr = JSON.parse(body);
        } catch (err) {
            jsonStr = null;
        }
        jsonStr ? res.send({"status":"fail", "command": jsonStr.command, "data": jsonStr.data}) : res.send({"status":"fail"});
    });
    res.end();
  }
})

let nodemailer = require('nodemailer')
let smtpTransport = require('nodemailer-smtp-transport')
async function sendMail(email, message, res) {
  // var name = req.body.name
  // var email = req.body.email
  console.log("/param post sendmail to ..." + email);
  let options = {
        service: 'gmail',
        secure: true,
        auth: {
          user: mail_account,
          pass: mail_password
        }
      }
  
  let mailTransport = nodemailer.createTransport(smtpTransport(options))
  let mail = {
      from: 'CyberTan Inc.',//'from',
      to: email,//'reach.hu@gmail.com',
      subject: 'CyberTan Inc.',
      html: message
    }
  let result_mail = await mailTransport.sendMail(mail, null)
    mailTransport.close()
    if (result_mail == 400) {
      // res.json(400, {error});
      res.status(400).json({error})
    } else {
      // res.json(200, {response: 'Mail sent.'});
      res.status(200).json({response: 'Mail sent.'})
    }
}

function asyn_sendMail(email, message, p_callback) {
  console.log("/param post sendmail to " + email);
  let options = {
        service: 'gmail',
        secure: true,
        auth: {
          user: mail_account,
          pass: mail_password
        }
      }
  
  let mailTransport = nodemailer.createTransport(smtpTransport(options))

  let mail = {
      from: 'CyberTan Inc.',//'from',
      to: email,//'reach.hu@gmail.com',
      subject: 'CyberTan Inc.',
      html: message
    }

    // var errorCode = await mailTransport.sendMail(mail, (error, response) => {
    mailTransport.sendMail(mail, (error, response) => {
      console.log("mailTransport.sendMail...return")
      mailTransport.close()
      if (error) {
        p_callback(400)
      } else {
        p_callback(200)
      }
    })
}

var promise_sendMail = function(email, message) {
  console.log("/param post sendmail to " + email);
  let options = {
        service: 'gmail',
        secure: true,
        auth: {
          user: mail_account,
          pass: mail_password
        }
      }
  
  let mailTransport = nodemailer.createTransport(smtpTransport(options))

  let mail = {
      from: 'CyberTan Inc.',//'from',
      to: 'reach.hu@gmail.com1',//email,//'reach.hu@gmail.com',
      subject: 'CyberTan Inc.',
      html: message
    }

    mailTransport.sendMail(mail, (error, response) => {
      mailTransport.close()
      if (error) {
        return Promise.resolve(400)
      } else {
        return Promise.resolve(200)
      }
    })
}

app.post('/cytertan_demo/verifyCode', function(req, res, next){

  if (req.body.command) {
    //能正确解析 json 格式的post参数
    var email = req.body.data.email;
    var verifyCode = req.body.data.verifyCode;
    console.log("verifyCode = "+verifyCode)
    console.log('req.body.command('+req.body.command+'):'+email+','+verifyCode);
    c.query('SELECT * FROM test_accounts where email = ?',[email], function(err, rows) {
      if (err){
        console.log('throw err!');
        res.send({"status": "fail"});
        res.end();
        // throw err;
        return;
      }
      console.log('Check row...');
      let numRows = rows.length;
      if (numRows != 0){
        console.log('rows = %s'+rows);
        var rows_email = rows[0].email
        var rows_verifyCode = rows[0].verifyCode
        if (verifyCode == rows_verifyCode){
          console.log("Verify code(%s) ok....", verifyCode);
          c.query("UPDATE test_accounts SET status = 1 WHERE id = ?",[rows[0].id], function(err, rows) {
            if (err){
              console.log('throw err!');
              res.send({
                "status": "fail",
                "desc":"update status fail"});
              res.end();
              // throw err;
              return;
            }
            res.send({"status": "success", 
                  "desc":"verify ok."});
            res.end();
          });
        }else{
          console.log('verifyCode(%s) is not matched', verifyCode);
          res.send({"status": "fail", 
                    "desc":"verifyCode is not matched."});
          res.end();
        }
      }else{
        console.log('account is not exist');
        res.send({"status": "fail", 
                  "desc":"account is not exist."});
        res.end();
      }
    });
    
} else {
    // 不能正确解析json 格式的post参数
    console.log('不能 - 正确解析json 格式的post参数');
    var body = '', jsonStr;
    req.on('data', function (chunk) {
        console.log("req.on(data)...triger"+chunk)
        body += chunk; //读取参数流转化为字符串
    });
    req.on('end', function () {
        //读取参数流结束后将转化的body字符串解析成 JSON 格式
        console.log("req.(end)...triger"+chunk)
        try {
            jsonStr = JSON.parse(body);
        } catch (err) {
            jsonStr = null;
        }
        jsonStr ? res.send({"status":"success", "command": jsonStr.command, "data": jsonStr.data}) : res.send({"status":"error"});
    });
    res.end();
  }
})

app.post('/cytertan_demo/forgetPassword', function(req, res, next){

  if (req.body.command) {
    //能正确解析 json 格式的post参数
    var email = req.body.data.email;
    console.log('req.body.command('+req.body.command+'):'+email);
    c.query('SELECT * FROM test_accounts where email = ?',[email], function(err, rows) {
      if (err){
        console.log('throw err!');
        res.send({"status": "fail"});
        res.end();
        throw err;
      }
      console.log('Check row...');
      let numRows = rows.length;
      if (numRows != 0){
        console.log('rows = %s',rows);
        if(rows[0].status == 0){
          console.log('account is not verified.');
          res.send({"status": "fail", 
                  "desc":"account is not verified."});
          res.end();
        }else if(rows[0].status == -1){
          console.log('account is deleted.');
          res.send({"status": "fail", 
                  "desc":"account is deleted."});
          res.end();
        }else{
          var rows_email = rows[0].email
          var rows_password = rows[0].password
          message = "Your password is - "+rows_password;
          asyn_sendMail(email, message, function(tmp_res){
            console.log('asyn_sendMail...return');
            if (tmp_res == 200){
              res.status(200).json({response: 'Mail sent.'});
            }else if (tmp_res == 400){
              res.status(400).json({response: 'Mail sent fail!'});
            }
            res.end();
          })
        }
      }else{
        console.log('account is not exist');
        res.send({"status": "fail", 
                  "desc":"account is not exist."});
        res.end();
      }
    });
    
} else {
    // 不能正确解析json 格式的post参数
    console.log('不能 - 正确解析json 格式的post参数');
    var body = '', jsonStr;
    req.on('data', function (chunk) {
        console.log("req.on(data)...triger"+chunk)
        body += chunk; //读取参数流转化为字符串
    });
    req.on('end', function () {
        //读取参数流结束后将转化的body字符串解析成 JSON 格式
        console.log("req.(end)...triger"+chunk)
        try {
            jsonStr = JSON.parse(body);
        } catch (err) {
            jsonStr = null;
        }
        jsonStr ? res.send({"status":"success", "command": jsonStr.command, "data": jsonStr.data}) : res.send({"status":"error"});
    });
    res.end();
  }
})

app.post('/cytertan_demo/listenDevice', function(req, res, next){

  if (req.body.command) {
    //能正确解析 json 格式的post参数
    var email = req.body.data.email;
    var deviceId = req.body.data.deviceId;
    var p_sessionId = req.body.session
    console.log("session = "+p_sessionId)
    console.log("deviceId = "+deviceId)
    console.log('req.body.command('+req.body.command+'):'+email+','+deviceId);
    if (p_sessionId == null) {
      console.log('throw err!');
        res.send({"status": "fail",
                  "desc":"not login"});
        res.end();
        return
    }

    c.query('SELECT * FROM test_accounts where email = ?',[email], function(err, rows) {
      if (err){
        console.log('throw err!');
        res.send({"status": "fail"});
        res.end();
        // throw err;
        return;
      }
      console.log('Check row...');
      let numRows = rows.length;
      if (numRows != 0){
        console.log('rows = %s',rows);
        if (rows[0].sessionId != p_sessionId){
          console.log('valid session is '+rows[0].sessionId + ' but receive session is '+p_sessionId);
          res.send({"status": "fail",
                  "desc":"session is invalid!"});
          res.end();
          return
        }

        if(rows[0].status == 0){
          console.log('account is not verified.');
          res.send({"status": "fail", 
                  "desc":"account is not verified."});
          res.end();
        }else if(rows[0].status == -1){
          console.log('account is deleted.');
          res.send({"status": "fail", 
                  "desc":"account is deleted."});
          res.end();
        }else if(rows[0].deviceId != deviceId){
          console.log('DeviceId is not matched.');
          res.send({"status": "fail", 
                  "desc":"DeviceId is not matched."});
          res.end();
        }else{
          console.log('Check ok - '+mqtt_host);
          res.send({"status": "success", 
                  "desc":"listene device ok.",
                  "data":{
                    "ip":mqtt_host,
                    "port":mqtt_port,
                    "topic":"devMsg/"+deviceId+"/notify",
                    "user":mqtt_account,
                    "password":mqtt_password
                  }
                });
          res.end();
        }
      }else{
        console.log('account is not exist');
        res.send({"status": "fail", 
                  "desc":"account is not exist."});
        res.end();
      }
    });
    
} else {
    // 不能正确解析json 格式的post参数
    console.log('不能 - 正确解析json 格式的post参数');
    var body = '', jsonStr;
    req.on('data', function (chunk) {
        console.log("req.on(data)...triger"+chunk)
        body += chunk; //读取参数流转化为字符串
    });
    req.on('end', function () {
        //读取参数流结束后将转化的body字符串解析成 JSON 格式
        console.log("req.(end)...triger"+chunk)
        try {
            jsonStr = JSON.parse(body);
        } catch (err) {
            jsonStr = null;
        }
        jsonStr ? res.send({"status":"success", "command": jsonStr.command, "data": jsonStr.data}) : res.send({"status":"error"});
    });
    res.end();
  }
})