var isMomHappy = true

var willIGetNewPhone = new Promise(function (resolve, reject) {
  if (isMomHappy) {
    var phone = {
      brand: 'Samsung',
      color: 'black',
      type: 's8'
    }
    resolve(phone)
  } else {
    var reason = new Error('Mom is unhappy')
    reject(reason)
  }
})

var func_phone = new Promise(function (resolve, reject) {
    var message = 'Android'
    resolve(message)
  })

var func_phone2 = function (message) {
    var p_message = message + ', Android2'
    return Promise.resolve(p_message)
  }

var showOff = function (phone) {
    // var message = 'Hey friend, I have a new ' + phone.color + ' ' + phone.brand + ' phone ' + phone.type
    var message = 'Hey friend, I have a new '+phone
    return Promise.resolve(message)
  }

var willIGetNewPhone2 = function () {
    if (isMomHappy) {
      var phone = {
        brand: 'Samsung',
        color: 'black',
        type: 's8'
      }
      return Promise.resolve(phone)
    } else {
      var reason = new Error('Mom is unhappy')
      return Promise.reject(reason)
    }
  }

var askMom = function () {
    // willIGetNewPhone2
    //   .then(showOff)
    func_phone.then(func_phone2)
      .then(function (fulfilled) {
        console.log(fulfilled)
        return Promise.resolve(fulfilled+"...2")
      })
      .then(function (fulfilled) {
        console.log(fulfilled)
      })
      .catch(function (error) {
        console.log(error.message)
      })
  }
  askMom()